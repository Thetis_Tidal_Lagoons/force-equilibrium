# -*- coding: utf-8 -*-
"""
Purpose: 2D Lineal Torsional Spring Solver
Author: T. M. McManus
Date: 21-9-16
"""

#import matplotlib
#matplotlib.use('Agg')
import matplotlib.cm as cm
import scipy as sp
import matplotlib.pyplot as plt
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import itertools
import time
import readline
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

readline.parse_and_bind("tab: complete")
  
#fileinput.input()
mesh_file = raw_input("Enter mesh file:")
#mesh_file = fileinput.filename()
print(mesh_file)
f = open(mesh_file,'r')
start = time.time()
plt.close('all')


#f = open('comp_he_10.msh', 'r')
#f = open(mesh_file,'r')
f.readline()
f.readline()
f.readline()
f.readline()

n_nodes = f.readline()
n_nodes.strip()
n_nodes = int(n_nodes)
node_loc =[]
for n in range(n_nodes):
    line = f.readline()
    line = line.strip()
    rows = line.split()
    rows[0] = int(rows[0])
    rows[1] = float(rows[1])
    rows[2] = float(rows[2])
    rows[3] = float(rows[3])
    node_loc.append(rows)
np_node_loc = sp.array(node_loc) 


f.readline()
f.readline()
n_ele = f.readline()
n_ele = n_ele.strip()
n_ele = int(n_ele)

ele = []
for m in range(n_ele):
    line = f.readline()
    line = line.strip()
    other_rows = line.split()
    for count in range(len(other_rows)):
       other_rows[count]=int(other_rows[count])
    ele.append(other_rows)
f.close()

#Go throught every element and determine if it is a triangle
#if it is not, remove it from the list of triangular elements

tri_ele_total = 0;
bound_ele_total = 0;
corner_ele_total=0;
tri_ele = []
bound_ele=[]
corner_ele=[]
for m in range(n_ele):
  if ele[m][1] == 2:
      tri_ele.append(ele[m][:])
      tri_ele_total = tri_ele_total + 1
  elif ele[m][1] == 1:
      bound_ele.append(ele[m][:])
      bound_ele_total = bound_ele_total + 1
  else: 
      corner_ele.append(ele[m][:])
      corner_ele_total = corner_ele_total + 1

boundary_points = []

#Determining N,S,W,E boundaries
#N = physical group 13, S = 11, W = 14, E = 12;
N_bound_ele = [];
S_bound_ele = [];
W_bound_ele = [];
E_bound_ele = [];
N_bound_ele_total = 0;
S_bound_ele_total = 0;
W_bound_ele_total = 0;
E_bound_ele_total = 0;

##Adapting to James' square mesh file    
##NOTE THIS IS PROBLEM SPECIFIC READ THE .GEO FILE to determine what are the
##correct boundary labels

for m in range(bound_ele_total):
    if bound_ele[m][3]==3:
        N_bound_ele.append(bound_ele[m][:])
        N_bound_ele_total = N_bound_ele_total+1;
    elif bound_ele[m][3]==1:
        S_bound_ele.append(bound_ele[m][:])
        S_bound_ele_total = S_bound_ele_total+1;
    elif bound_ele[m][3]==2:
        E_bound_ele.append(bound_ele[m][:])
        E_bound_ele_total = E_bound_ele_total+1;
    else:
        W_bound_ele.append(bound_ele[m][:])
        W_bound_ele_total = W_bound_ele_total +1;

for m in range(bound_ele_total):
    boundary_points.append(bound_ele[m][-2:])

N_boundary_points = [];
S_boundary_points = [];
W_boundary_points = [];
E_boundary_points = [];
    
for m in range(N_bound_ele_total):
    N_boundary_points.append(N_bound_ele[m][-2:])
for m in range(S_bound_ele_total):
    S_boundary_points.append(S_bound_ele[m][-2:])
for m in range(W_bound_ele_total):
    W_boundary_points.append(W_bound_ele[m][-2:])
for m in range(E_bound_ele_total):
    E_boundary_points.append(E_bound_ele[m][-2:])

#print(boundary_points)
#Remove duplicates
#Flatten boundary_points
flattened = list(itertools.chain.from_iterable(boundary_points))
N_flat = list(itertools.chain.from_iterable(N_boundary_points))
S_flat = list(itertools.chain.from_iterable(S_boundary_points))
W_flat = list(itertools.chain.from_iterable(W_boundary_points))
E_flat = list(itertools.chain.from_iterable(E_boundary_points))

N_flat = list(set(sorted(N_flat)))
S_flat = list(set(sorted(S_flat)))
W_flat = list(set(sorted(W_flat)))
E_flat = list(set(sorted(E_flat)))

np_N_bound_points = sp.array(N_flat)-1
np_S_bound_points = sp.array(S_flat)-1
np_W_bound_points = sp.array(W_flat)-1
np_E_bound_points = sp.array(E_flat)-1

#Keep in mind that boundary elements DO NOT LINE UP with ARRAY INDEXING and this step needs to be added
boundary_points = list(set(sorted(flattened)))
np_boundary_points = sp.array(boundary_points)-1

n_nodes_ext = np_boundary_points.size
n_nodes_int = n_nodes - n_nodes_ext

node_int_loc = sp.delete(np_node_loc,np_boundary_points,0)


#Only interested in x and y coords though, so this can be cleaned up
np_comp_boundary_nodes=np_boundary_points
np_comp_node_loc = np_node_loc[np_comp_boundary_nodes,:]

np_N_comp_node_loc = np_node_loc[np_N_bound_points,:]
np_S_comp_node_loc = np_node_loc[np_S_bound_points,:]
np_W_comp_node_loc = np_node_loc[np_W_bound_points,:]
np_E_comp_node_loc = np_node_loc[np_E_bound_points,:]


np_phys_N = np_N_comp_node_loc.copy()
np_phys_S = np_S_comp_node_loc.copy()
np_phys_W = np_W_comp_node_loc.copy()
np_phys_E = np_E_comp_node_loc.copy()

np_tri_ele = sp.array(tri_ele)
np_phys_sine = np_comp_node_loc.copy()


tri_ele_total = np_tri_ele.shape[0]

def neb(node_int_number):
    neb_a = np_tri_ele[sp.where(np_tri_ele[:,-3]==node_int_number)][:,[-2,-1]]
    neb_b = np_tri_ele[sp.where(np_tri_ele[:,-2]==node_int_number)][:,[-3,-1]]
    neb_c = np_tri_ele[sp.where(np_tri_ele[:,-1]==node_int_number)][:,[-3,-2]]
    neb_fill = sp.unique(sp.concatenate((neb_a,neb_b,neb_c),axis=0))
    if neb_fill.size==3:
        neb_fill=sp.append(neb_fill,[0,0,0,0,0])    
    elif neb_fill.size==4:
        neb_fill=sp.append(neb_fill,[0,0,0,0])
    elif neb_fill.size==5:
        neb_fill=sp.append(neb_fill,[0,0,0])
    elif neb_fill.size==6:
        neb_fill=sp.append(neb_fill,[0,0])
    elif neb_fill.size==7:
        neb_fill=sp.append(neb_fill,[0])
    return neb_fill

def lij(nodei,nodej):
    ix,iy=np_node_loc[sp.where(np_node_loc[:,0]==nodei),[1,2]].flatten()
    jx,jy=np_node_loc[sp.where(np_node_loc[:,0]==nodej),[1,2]].flatten()
    length = sp.sqrt((ix-jx)**2 + (iy-jy)**2)
    return length 

def area(nodei_index,nodej_index,nodek_index):
    s = (lij(nodei_index,nodej_index) + lij(nodei_index,nodek_index) + lij(nodej_index,nodek_index))/2.0
    area = sp.sqrt(s*(s-lij(nodei_index,nodej_index))*(s-lij(nodei_index,nodek_index))*(s-lij(nodej_index,nodek_index)))
    return area
    
def tors(nodei,nodej,nodek):
    i=nodei;j=nodej;k=nodek;
    tors = (lij(i,j)**2 * lij(i,k)**2)/(4*area(i,j,k)**2)
    return tors
    
def K_lin(nodei,nodej):
    K_lin = sp.zeros((4,4))
    ix,iy=np_node_loc[sp.where(np_node_loc[:,0]==nodei),[1,2]].flatten()
    jx,jy=np_node_loc[sp.where(np_node_loc[:,0]==nodej),[1,2]].flatten()
    y_edge = jy-iy; x_edge = jx-ix;
    alpha = sp.arctan2(y_edge,x_edge)
    
    K_lin[0,0]=(sp.cos(alpha))**2
    K_lin[0,1]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[0,2]=-1.0*(sp.cos(alpha))**2
    K_lin[0,3]=-1.0*sp.sin(alpha)*sp.cos(alpha)


    K_lin[1,0]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[1,1]=(sp.sin(alpha))**2
    K_lin[1,2]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[1,3]=-1.0*(sp.sin(alpha))**2
    
    K_lin[2,0]=-1.0*(sp.cos(alpha))**2
    K_lin[2,1]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[2,2]=(sp.cos(alpha))**2
    K_lin[2,3]=sp.sin(alpha)*sp.cos(alpha)
    
    
    K_lin[3,0]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[3,1]=-1.0*(sp.sin(alpha))**2
    K_lin[3,2]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[3,3]=(sp.sin(alpha))**2

    K_lin = (1.0/lij(nodei,nodej))*K_lin
    
    return K_lin

def neb_tri(nodei,nodej):
    if nodei == nodej:
        print ("nodei and nodej must be distinct.\n")
        return sp.NaN
    #For edges along a boundary there is only one triangle per edge
    elif sp.any(sp.array(bound_ele)[:,0]==nodei) and sp.any(sp.array(bound_ele)[:,0]==nodej):
        k1=sp.trim_zeros(sp.intersect1d(neb(nodei),neb(nodej)))[0]
        K_tor1 = K_tor(nodei,nodej,k1)[[0,1,2,3],:]
        return k1,K_tor1
    #For edges on the interior there are two triangles per edge
    else:
        k1,k2=sp.array(sp.trim_zeros(sp.intersect1d(neb(nodei),neb(nodej)))).flatten()
        K_tor1 = K_tor(nodei,nodej,k1)[[0,1,2,3],:]
        K_tor2 = K_tor(nodei,nodej,k2)[[0,1,2,3],:]
        return k1,K_tor1,k2,K_tor2

def C(nodei,nodej,nodek):
    C_mat = sp.zeros((3,3))
    i=nodei;j=nodej;k=nodek;
    C_mat[0,0]=tors(i,j,k)
    C_mat[1,1]=tors(j,k,i)
    C_mat[2,2]=tors(k,i,j)
    return C_mat

def K_tor(nodei,nodej,nodek):
    i=nodei;j=nodej;k=nodek;
    A = sp.dot(sp.transpose(rot(i,j,k)),C(i,j,k))
    K_tor = sp.dot(A,rot(i,j,k))
    return K_tor

def int_angle(nodei,nodej,nodek):
    A = lij(nodei,nodej)**2 + lij(nodei,nodek)**2 - lij(nodej,nodek)**2
    B = 2*lij(nodei,nodej)*lij(nodei,nodek)
    int_angle = sp.arccos(A/B)
    return int_angle
    
def min_int_angle(np_tri_ele):
    min_int_angle = sp.zeros(np_tri_ele.shape[0])
    tri_int_holder = sp.zeros(3)
    triangles=np_tri_ele[:,[-3,-2,-1]]
    
    for n in range(np_tri_ele.shape[0]):
        tri_int_holder[0]=int_angle(triangles[n,0],triangles[n,1],triangles[n,2])
        tri_int_holder[1]=int_angle(triangles[n,1],triangles[n,2],triangles[n,0])
        tri_int_holder[2]=int_angle(triangles[n,2],triangles[n,0],triangles[n,1])
        min_int_angle[n]=tri_int_holder.min()
    return min_int_angle

def signed_area(nodei,nodej,nodek):
    x1,y1=np_node_loc[sp.where(np_node_loc[:,0]==nodei),[1,2]].flatten()
    x2,y2=np_node_loc[sp.where(np_node_loc[:,0]==nodej),[1,2]].flatten()
    x3,y3=np_node_loc[sp.where(np_node_loc[:,0]==nodek),[1,2]].flatten()
    A = 0.5*(-1.0*x2*y1 + x3*y1 + x1*y2 - x3*y2 - x1*y3 + x2*y3)
    return A 

def tot_signed_area(np_tri_ele):
    tot_signed_area = sp.zeros(np_tri_ele.shape[0])
    triangles = np_tri_ele[:,[-3,-2,-1]]
    for n in range(np_tri_ele.shape[0]):
        tot_signed_area[n]=signed_area(triangles[n][0],triangles[n][1],triangles[n][2])
    return tot_signed_area


def x_disp(nodei,nodej):
    x_hold=sp.zeros(2)
    x_hold[0]=np_node_loc[sp.where(np_node_loc[:,0]==nodei),1].flatten()[0]
    x_hold[1]=np_node_loc[sp.where(np_node_loc[:,0]==nodej),1].flatten()[0]
    x_disp = x_hold[1]-x_hold[0]
    return x_disp

def y_disp(nodei,nodej):
    y_hold=sp.zeros(2)
    y_hold[0]=np_node_loc[sp.where(np_node_loc[:,0]==nodei),2].flatten()[0]
    y_hold[1]=np_node_loc[sp.where(np_node_loc[:,0]==nodej),2].flatten()[0]
    y_disp = y_hold[1]-y_hold[0]
    return y_disp
    
def a_rot(nodei,nodej):
    a_rot = x_disp(nodei,nodej)/(lij(nodei,nodej)**2)
    return a_rot

def b_rot(nodei,nodej):
    b_rot = y_disp(nodei,nodej)/(lij(nodei,nodej)**2)
    return b_rot

def rot(nodei,nodej,nodek):
    rot_mat = sp.zeros((3,6))
    i=nodei;j=nodej;k=nodek;
    
    rot_mat[0,0]=b_rot(i,k)-b_rot(i,j)
    rot_mat[0,1]=a_rot(i,j)-a_rot(i,k)
    rot_mat[0,2]=b_rot(i,j)
    rot_mat[0,3]=-1.0*a_rot(i,j)
    rot_mat[0,4]=-1.0*b_rot(i,k)
    rot_mat[0,5]=a_rot(i,k)
    
    rot_mat[1,0]=-1.0*b_rot(j,i)
    rot_mat[1,1]=a_rot(j,i)
    rot_mat[1,2]=b_rot(j,i)-b_rot(j,k)
    rot_mat[1,3]=a_rot(j,k)-a_rot(j,i)
    rot_mat[1,4]=b_rot(j,k)
    rot_mat[1,5]=-1.0*a_rot(j,k)

    rot_mat[2,0]=b_rot(k,i)
    rot_mat[2,1]=-1.0*a_rot(k,i)
    rot_mat[2,2]=-1.0*b_rot(k,j)
    rot_mat[2,3]=a_rot(k,j)
    rot_mat[2,4]=b_rot(k,j)-b_rot(k,i)
    rot_mat[2,5]=a_rot(k,i)-a_rot(k,j)
    
    return rot_mat
        
def K_lin_loc(nodei):#Lineal+Torsional stiffness at a given node
    nodei=int(nodei)
    K_lin_loc = sp.zeros([n_nodes*2,n_nodes*2])
    
    for n in neb(nodei)[sp.nonzero(neb(nodei))[0]].astype(int):
        
        K_lin_loc[2*nodei-2,2*nodei-2]=K_lin(nodei,n)[0,0] + K_lin_loc[2*nodei-2,2*nodei-2]
        K_lin_loc[2*nodei-2,2*nodei-1]=K_lin(nodei,n)[0,1] + K_lin_loc[2*nodei-2,2*nodei-1]
        K_lin_loc[2*nodei-2,(n*2)-2]=K_lin(nodei,n)[0,2] + K_lin_loc[2*nodei-2,(n*2)-2]
        K_lin_loc[2*nodei-2,(n*2)-1]=K_lin(nodei,n)[0,3] + K_lin_loc[2*nodei-2,(n*2)-1]
        
        K_lin_loc[2*nodei-1,2*nodei-2]=K_lin(nodei,n)[1,0] + K_lin_loc[2*nodei-1,2*nodei-2]
        K_lin_loc[2*nodei-1,2*nodei-1]=K_lin(nodei,n)[1,1] + K_lin_loc[2*nodei-1,2*nodei-1]
        K_lin_loc[2*nodei-1,(n*2)-2]=K_lin(nodei,n)[1,2] + K_lin_loc[2*nodei-1,(n*2)-2]
        K_lin_loc[2*nodei-1,(n*2)-1]=K_lin(nodei,n)[1,3] + K_lin_loc[2*nodei-1,(n*2)-1]
        
        K_lin_loc[2*n-2,2*nodei-2]=K_lin(nodei,n)[2,0] + K_lin_loc[2*n-2,2*nodei-2]
        K_lin_loc[2*n-2,2*nodei-1]=K_lin(nodei,n)[2,1] + K_lin_loc[2*n-2,2*nodei-1]
        K_lin_loc[2*n-2,(n*2)-2]=K_lin(nodei,n)[2,2] + K_lin_loc[2*n-2,(n*2)-2]
        K_lin_loc[2*n-2,(n*2)-1]=K_lin(nodei,n)[2,3] + K_lin_loc[2*n-2,(n*2)-1]
        
        K_lin_loc[2*n-1,2*nodei-2]=K_lin(nodei,n)[3,0] + K_lin_loc[2*n-1,2*nodei-2]
        K_lin_loc[2*n-1,2*nodei-1]=K_lin(nodei,n)[3,1] + K_lin_loc[2*n-1,2*nodei-1]
        K_lin_loc[2*n-1,(n*2)-2]=K_lin(nodei,n)[3,2] + K_lin_loc[2*n-1,(n*2)-2]
        K_lin_loc[2*n-1,(n*2)-1]=K_lin(nodei,n)[3,3] + K_lin_loc[2*n-1,(n*2)-1]
        
        #Add contributions from Torsion
          
        if len(neb_tri(nodei,n))==2:
            k1 = neb_tri(nodei,n)[0].astype(int)
              
            K_lin_loc[2*nodei-2,2*nodei-2]=neb_tri(nodei,n)[1][0,0] + K_lin_loc[2*nodei-2,2*nodei-2]
            K_lin_loc[2*nodei-2,2*nodei-1]=neb_tri(nodei,n)[1][0,1] + K_lin_loc[2*nodei-2,2*nodei-1]
            K_lin_loc[2*nodei-2,(n*2)-2]=neb_tri(nodei,n)[1][0,2] + K_lin_loc[2*nodei-2,(n*2)-2]
            K_lin_loc[2*nodei-2,(n*2)-1]=neb_tri(nodei,n)[1][0,3] + K_lin_loc[2*nodei-2,(n*2)-1]
            K_lin_loc[2*nodei-2,(k1*2)-2]=neb_tri(nodei,n)[1][0,4] + K_lin_loc[2*nodei-2,(k1*2)-2]
            K_lin_loc[2*nodei-2,(k1*2)-1]=neb_tri(nodei,n)[1][0,5] + K_lin_loc[2*nodei-2,(k1*2)-1] 
            
            K_lin_loc[2*nodei-1,2*nodei-2]=neb_tri(nodei,n)[1][1,0] + K_lin_loc[2*nodei-1,2*nodei-2]
            K_lin_loc[2*nodei-1,2*nodei-1]=neb_tri(nodei,n)[1][1,1] + K_lin_loc[2*nodei-1,2*nodei-1]
            K_lin_loc[2*nodei-1,(n*2)-2]=neb_tri(nodei,n)[1][1,2] + K_lin_loc[2*nodei-1,(n*2)-2]
            K_lin_loc[2*nodei-1,(n*2)-1]=neb_tri(nodei,n)[1][1,3] + K_lin_loc[2*nodei-1,(n*2)-1]             
            K_lin_loc[2*nodei-1,(k1*2)-2]=neb_tri(nodei,n)[1][1,4] + K_lin_loc[2*nodei-1,(k1*2)-2]
            K_lin_loc[2*nodei-1,(k1*2)-1]=neb_tri(nodei,n)[1][1,5] + K_lin_loc[2*nodei-1,(k1*2)-1]
            
            K_lin_loc[2*n-2,2*nodei-2]=neb_tri(nodei,n)[1][2,0] + K_lin_loc[2*n-2,2*nodei-2]
            K_lin_loc[2*n-2,2*nodei-1]=neb_tri(nodei,n)[1][2,1] + K_lin_loc[2*n-2,2*nodei-1]
            K_lin_loc[2*n-2,(n*2)-2]=neb_tri(nodei,n)[1][2,2] + K_lin_loc[2*n-2,(n*2)-2]
            K_lin_loc[2*n-2,(n*2)-1]=neb_tri(nodei,n)[1][2,3] + K_lin_loc[2*n-2,(n*2)-1]
            K_lin_loc[2*n-2,(k1*2)-2]=neb_tri(nodei,n)[1][2,4] + K_lin_loc[2*n-2,(k1*2)-2]
            K_lin_loc[2*n-2,(k1*2)-1]=neb_tri(nodei,n)[1][2,5] + K_lin_loc[2*n-2,(k1*2)-1] 
            
            K_lin_loc[2*n-1,2*nodei-2]=neb_tri(nodei,n)[1][3,0] + K_lin_loc[2*n-1,2*nodei-2]
            K_lin_loc[2*n-1,2*nodei-1]=neb_tri(nodei,n)[1][3,1] + K_lin_loc[2*n-1,2*nodei-1]
            K_lin_loc[2*n-1,(n*2)-2]=neb_tri(nodei,n)[1][3,2] + K_lin_loc[2*n-1,(n*2)-2]
            K_lin_loc[2*n-1,(n*2)-1]=neb_tri(nodei,n)[1][3,3] + K_lin_loc[2*n-1,(n*2)-1]             
            K_lin_loc[2*n-1,(k1*2)-2]=neb_tri(nodei,n)[1][3,4] + K_lin_loc[2*n-1,(k1*2)-2]
            K_lin_loc[2*n-1,(k1*2)-1]=neb_tri(nodei,n)[1][3,5] + K_lin_loc[2*n-1,(k1*2)-1]
            
            
        else:
            k1 = neb_tri(nodei,n)[0]
            k2 = neb_tri(nodei,n)[2]
            
            K_lin_loc[2*nodei-2,2*nodei-2]=neb_tri(nodei,n)[1][0,0] + K_lin_loc[2*nodei-2,2*nodei-2]
            K_lin_loc[2*nodei-2,2*nodei-1]=neb_tri(nodei,n)[1][0,1] + K_lin_loc[2*nodei-2,2*nodei-1]
            K_lin_loc[2*nodei-2,(n*2)-2]=neb_tri(nodei,n)[1][0,2] + K_lin_loc[2*nodei-2,(n*2)-2]
            K_lin_loc[2*nodei-2,(n*2)-1]=neb_tri(nodei,n)[1][0,3] + K_lin_loc[2*nodei-2,(n*2)-1]
            K_lin_loc[2*nodei-2,(k1*2)-2]=neb_tri(nodei,n)[1][0,4] + K_lin_loc[2*nodei-2,(k1*2)-2]
            K_lin_loc[2*nodei-2,(k1*2)-1]=neb_tri(nodei,n)[1][0,5] + K_lin_loc[2*nodei-2,(k1*2)-1] 
            
            K_lin_loc[2*nodei-1,2*nodei-2]=neb_tri(nodei,n)[1][1,0] + K_lin_loc[2*nodei-1,2*nodei-2]
            K_lin_loc[2*nodei-1,2*nodei-1]=neb_tri(nodei,n)[1][1,1] + K_lin_loc[2*nodei-1,2*nodei-1]
            K_lin_loc[2*nodei-1,(n*2)-2]=neb_tri(nodei,n)[1][1,2] + K_lin_loc[2*nodei-1,(n*2)-2]
            K_lin_loc[2*nodei-1,(n*2)-1]=neb_tri(nodei,n)[1][1,3] + K_lin_loc[2*nodei-1,(n*2)-1]             
            K_lin_loc[2*nodei-1,(k1*2)-2]=neb_tri(nodei,n)[1][1,4] + K_lin_loc[2*nodei-1,(k1*2)-2]
            K_lin_loc[2*nodei-1,(k1*2)-1]=neb_tri(nodei,n)[1][1,5] + K_lin_loc[2*nodei-1,(k1*2)-1]
            
            K_lin_loc[2*n-2,2*nodei-2]=neb_tri(nodei,n)[1][2,0] + K_lin_loc[2*n-2,2*nodei-2]
            K_lin_loc[2*n-2,2*nodei-1]=neb_tri(nodei,n)[1][2,1] + K_lin_loc[2*n-2,2*nodei-1]
            K_lin_loc[2*n-2,(n*2)-2]=neb_tri(nodei,n)[1][2,2] + K_lin_loc[2*n-2,(n*2)-2]
            K_lin_loc[2*n-2,(n*2)-1]=neb_tri(nodei,n)[1][2,3] + K_lin_loc[2*n-2,(n*2)-1]
            K_lin_loc[2*n-2,(k1*2)-2]=neb_tri(nodei,n)[1][2,4] + K_lin_loc[2*n-2,(k1*2)-2]
            K_lin_loc[2*n-2,(k1*2)-1]=neb_tri(nodei,n)[1][2,5] + K_lin_loc[2*n-2,(k1*2)-1] 
            
            K_lin_loc[2*n-1,2*nodei-2]=neb_tri(nodei,n)[1][3,0] + K_lin_loc[2*n-1,2*nodei-2]
            K_lin_loc[2*n-1,2*nodei-1]=neb_tri(nodei,n)[1][3,1] + K_lin_loc[2*n-1,2*nodei-1]
            K_lin_loc[2*n-1,(n*2)-2]=neb_tri(nodei,n)[1][3,2] + K_lin_loc[2*n-1,(n*2)-2]
            K_lin_loc[2*n-1,(n*2)-1]=neb_tri(nodei,n)[1][3,3] + K_lin_loc[2*n-1,(n*2)-1]             
            K_lin_loc[2*n-1,(k1*2)-2]=neb_tri(nodei,n)[1][3,4] + K_lin_loc[2*n-1,(k1*2)-2]
            K_lin_loc[2*n-1,(k1*2)-1]=neb_tri(nodei,n)[1][3,5] + K_lin_loc[2*n-1,(k1*2)-1]
            
            
            K_lin_loc[2*nodei-2,2*nodei-2]=neb_tri(nodei,n)[3][0,0] + K_lin_loc[2*nodei-2,2*nodei-2]
            K_lin_loc[2*nodei-2,2*nodei-1]=neb_tri(nodei,n)[3][0,1] + K_lin_loc[2*nodei-2,2*nodei-1]
            K_lin_loc[2*nodei-2,(n*2)-2]=neb_tri(nodei,n)[3][0,2] + K_lin_loc[2*nodei-2,(n*2)-2]
            K_lin_loc[2*nodei-2,(n*2)-1]=neb_tri(nodei,n)[3][0,3] + K_lin_loc[2*nodei-2,(n*2)-1]
            K_lin_loc[2*nodei-2,(k2*2)-2]=neb_tri(nodei,n)[3][0,4] + K_lin_loc[2*nodei-2,(k2*2)-2]
            K_lin_loc[2*nodei-2,(k2*2)-1]=neb_tri(nodei,n)[3][0,5] + K_lin_loc[2*nodei-2,(k2*2)-1] 
            
            K_lin_loc[2*nodei-1,2*nodei-2]=neb_tri(nodei,n)[3][1,0] + K_lin_loc[2*nodei-1,2*nodei-2]
            K_lin_loc[2*nodei-1,2*nodei-1]=neb_tri(nodei,n)[3][1,1] + K_lin_loc[2*nodei-1,2*nodei-1]
            K_lin_loc[2*nodei-1,(n*2)-2]=neb_tri(nodei,n)[3][1,2] + K_lin_loc[2*nodei-1,(n*2)-2]
            K_lin_loc[2*nodei-1,(n*2)-1]=neb_tri(nodei,n)[3][1,3] + K_lin_loc[2*nodei-1,(n*2)-1]             
            K_lin_loc[2*nodei-1,(k2*2)-2]=neb_tri(nodei,n)[3][1,4] + K_lin_loc[2*nodei-1,(k2*2)-2]
            K_lin_loc[2*nodei-1,(k2*2)-1]=neb_tri(nodei,n)[3][1,5] + K_lin_loc[2*nodei-1,(k2*2)-1]
            
            K_lin_loc[2*n-2,2*nodei-2]=neb_tri(nodei,n)[3][2,0] + K_lin_loc[2*n-2,2*nodei-2]
            K_lin_loc[2*n-2,2*nodei-1]=neb_tri(nodei,n)[3][2,1] + K_lin_loc[2*n-2,2*nodei-1]
            K_lin_loc[2*n-2,(n*2)-2]=neb_tri(nodei,n)[3][2,2] + K_lin_loc[2*n-2,(n*2)-2]
            K_lin_loc[2*n-2,(n*2)-1]=neb_tri(nodei,n)[3][2,3] + K_lin_loc[2*n-2,(n*2)-1]
            K_lin_loc[2*n-2,(k2*2)-2]=neb_tri(nodei,n)[3][2,4] + K_lin_loc[2*n-2,(k2*2)-2]
            K_lin_loc[2*n-2,(k2*2)-1]=neb_tri(nodei,n)[3][2,5] + K_lin_loc[2*n-2,(k2*2)-1] 
            
            K_lin_loc[2*n-1,2*nodei-2]=neb_tri(nodei,n)[3][3,0] + K_lin_loc[2*n-1,2*nodei-2]
            K_lin_loc[2*n-1,2*nodei-1]=neb_tri(nodei,n)[3][3,1] + K_lin_loc[2*n-1,2*nodei-1]
            K_lin_loc[2*n-1,(n*2)-2]=neb_tri(nodei,n)[3][3,2] + K_lin_loc[2*n-1,(n*2)-2]
            K_lin_loc[2*n-1,(n*2)-1]=neb_tri(nodei,n)[3][3,3] + K_lin_loc[2*n-1,(n*2)-1]             
            K_lin_loc[2*n-1,(k2*2)-2]=neb_tri(nodei,n)[3][3,4] + K_lin_loc[2*n-1,(k2*2)-2]
            K_lin_loc[2*n-1,(k2*2)-1]=neb_tri(nodei,n)[3][3,5] + K_lin_loc[2*n-1,(k2*2)-1]
        
    #return sp.sparse.lil_matrix(K_lin_loc)
    return K_lin_loc

triangles = np_tri_ele[:,-3:]-1.0
np_node_loc_orig=np_node_loc.copy()
steps=2

np_phys_N_prev = np_phys_N[:,2].copy()
w_0=sp.linspace(0,0.15,steps)
signed_area_prev = tot_signed_area(np_tri_ele)
tangle_counter=0

for counter in range(steps):

    np_phys_N[:,2] = w_0[counter]*sp.sin(2*sp.pi*np_phys_N[:,1])+1 #Boundary deformation

    np_phys_boundary = sp.concatenate((np_phys_S,np_phys_E,np_phys_N,np_phys_W),axis=0)
    np_phys_boundary = np_phys_boundary[np_phys_boundary[:,0].argsort()]
    np_phys_boundary = sp.delete(np_phys_boundary,[1,3,5,7],0)#These are the corners that never move

    disp = sp.zeros([n_nodes,2])

    n = 0
    for m in np_phys_N[:,0].astype(int):
        disp[m-1,0]=0.0
        disp[m-1,1]=np_phys_N[n,2] - np_phys_N_prev[n] #Defining the displacement
        n=n+1

    #print("Counter: {}\n".format(counter))
    f_glob = disp.copy()
    f_glob = sp.ravel(sp.column_stack((f_glob[:,0],f_glob[:,1])))
    K_glob = sp.zeros([2*n_nodes,2*n_nodes])
    #K_glob = sp.sparse.lil_matrix(K_glob)

   # if counter == 0:
    for n in sp.linspace(1,n_nodes,n_nodes):#Suboptimal assembly but it works
        #print("Node: {}".format(n))
        K_glob = K_lin_loc(n)+K_glob


    K_glob_ones = K_glob.copy()
    K_glob_ones[0:2*n_nodes_ext,:]=0.0

    for n in range(2*n_nodes_ext):
        K_glob_ones[n,n]=1.0

    orig_disp = sp.linalg.solve(K_glob_ones,f_glob) #Different ways to solve this can be experimented with
    #orig_disp = sp.sparse.linalg.spsolve(K_glob_ones,f_glob)
    solve_disp_reshape = orig_disp.reshape([n_nodes,2])

    np_node_loc[:,1]=np_node_loc[:,1]+solve_disp_reshape[:,0] #Smoothed X-coordinates
    np_node_loc[:,2]=np_node_loc[:,2]+solve_disp_reshape[:,1] #Smoothed Y-coordinates


    diff_signed_area = sp.sign(tot_signed_area(np_tri_ele))-sp.sign(signed_area_prev)
    if sp.any(diff_signed_area)!=0 and tangle_counter==0:
        tangle_counter=counter
        
    ##UNCOMMENT IF A FIGURE AT EVERY STEP IS DESIRED
    # plt.figure()
    # plt.triplot(np_node_loc[:,1],np_node_loc[:,2],'-ko',triangles = triangles,markersize=2)
    # plt.tripcolor(np_node_loc[:,1],np_node_loc[:,2],triangles=triangles,facecolors=min_int_angle(np_tri_ele),cmap=cm.rainbow_r,vmin=0,vmax=sp.pi/3)
    # cbar = plt.colorbar(ticks=[0, sp.pi/12,sp.pi/6,sp.pi/4, sp.pi/3])
    # cbar.ax.set_yticklabels([r'$0$', r'$\frac{\pi}{12}$',r'$\frac{\pi}{6}$',r'$\frac{\pi}{4}$',r'$\frac{\pi}{3}$'],fontsize=14)  # vertically oriented colorbar
    
    # plt.axes().set_aspect('equal')
    # plt.xlim(-0.1,1.1)
    # plt.ylim(-0.1,1.5)
    # plt.xlabel(r'$X$',fontsize=16)
    # plt.ylabel(r'$Y$',fontsize=16)
    # plt.title(r'$\textrm{S}_{l+t}$',fontsize=22)
    # plt.text(0.7,1.3,r'$n = {}$'.format(counter), fontsize=14)
    # if tangle_counter != 0:
    #     plt.text(0.7,1.2,r'$n_{} = {}$'.format('t',tangle_counter),fontsize=14)
    # plt.savefig('./figures/2d_lin_tor_direct_{}.png'.format(counter),bbox_inches='tight',dpi = 300)
    np_phys_N_prev = np_phys_N[:,2].copy()
#    plt.show()
    signed_area_prev=tot_signed_area(np_tri_ele)

plt.figure()
plt.triplot(np_node_loc[:,1],np_node_loc[:,2],'-ko',triangles = triangles,markersize=2)
plt.tripcolor(np_node_loc[:,1],np_node_loc[:,2],triangles=triangles,facecolors=min_int_angle(np_tri_ele),cmap=cm.rainbow_r,vmin=0,vmax=sp.pi/3)
#cbar = plt.colorbar(ticks=[0, sp.pi/12,sp.pi/6,sp.pi/4, sp.pi/3])
#cbar.ax.set_yticklabels([r'$0$', r'$\frac{\pi}{12}$',r'$\frac{\pi}{6}$',r'$\frac{\pi}{4}$',r'$\frac{\pi}{3}$'],fontsize=14)  # vertically oriented colorbar
plt.axes().set_aspect('equal')
plt.xlim(-0.1,1.1)
plt.ylim(-0.1,1.5)
plt.xlabel(r'$X$',fontsize=16)
plt.ylabel(r'$Y$',fontsize=16)
plt.title(r'$\textrm{S}_{l+t}$',fontsize=22)
plt.text(0.7,1.3,r'$n = {}$'.format(counter), fontsize=14)
if tangle_counter != 0:
    plt.text(0.7,1.2,r'$n_{} = {}$'.format('t',tangle_counter),fontsize=14)
plt.savefig('2d_lin_tor_direct_{}.png'.format(counter),bbox_inches='tight',dpi = 300)
