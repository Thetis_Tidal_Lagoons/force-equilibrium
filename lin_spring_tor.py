# -*- coding: utf-8 -*-
"""
Purpose: 2D Lineal Torsional Spring Solver
Authors: T. M. McManus, T. A. French
Date: 25-8-17
"""

#import matplotlib
#matplotlib.use('Agg')
import matplotlib.cm as cm
import scipy as sp
import matplotlib.pyplot as plt
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import itertools
import time
import readline
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

readline.parse_and_bind("tab: complete")

class Mesh:

    def __init__(self, file_name):
        self.tangle_counter = 0
        #fileinput.input()
        filename = raw_input("Enter mesh file:")
        #mesh_file = fileinput.filename()
        self.parse_file(filename)

        self.sort_elements()

        #Determining N,S,W,E boundaries
        #N = physical group 3, S = 1, W = 4, E = 2;
        #Pulls out lines of np_node_loc corresponding to physical groupID
        #Also shows locations of nodes compared to np_N_bound_points
        np_N_comp_node_loc = self.np_node_loc[self.get_boundary_points(10),:]
        # np_S_comp_node_loc = self.np_node_loc[self.get_boundary_points(1),:]
        #np_E_comp_node_loc = self.np_node_loc[self.get_boundary_points(2),:]
        # np_W_comp_node_loc = self.np_node_loc[self.get_boundary_points(4),:]
        np_comp_node_loc = self.np_node_loc[self.get_boundary_points(),:]

        self.np_phys_N = np_N_comp_node_loc.copy()
        # self.np_phys_S = np_S_comp_node_loc.copy()
        # self.np_phys_W = np_W_comp_node_loc.copy()
        # self.np_phys_E = np_E_comp_node_loc.copy()

        self.np_phys_N_prev = self.np_phys_N[:,1:3].copy()

        #Calculate number of exterior (boundary) and internal points
        self.n_nodes_ext = self.get_boundary_points().size
        self.n_nodes_int = self.n_nodes - self.n_nodes_ext

        #Removes locations of boundary points from a copy of np_node_loc to give locations of interior points
        self.node_int_loc = sp.delete(self.np_node_loc,self.get_boundary_points(),0)

        #Creates copy of tri_ele as a numpy array
        self.np_tri_ele = sp.array(self.tri_ele)

        start = time.time()
        plt.close('all')

    def iterate(self, steps):
        self.signed_area_prev = self.tot_signed_area()
        for self.counter in range(steps):
            self.relax_mesh()
            self.tangle_check()
            self.plot_mesh(self.counter)

    #################################
    ## Mesh Construction
    #################################
    def parse_file(self, filename):
        mesh_file = open(filename,'r')
        # Strip off preamble
        mesh_file.readline()
        mesh_file.readline()
        mesh_file.readline()
        mesh_file.readline()

        self.parse_nodes(mesh_file)

        #Strip $EndNodes
        mesh_file.readline()
        #Strip $Elements
        mesh_file.readline()

        self.parse_elements(mesh_file)
        mesh_file.close()

    def parse_nodes(self, mesh_file):
        #Read the number of nodes and cast to int
        n_nodes = mesh_file.readline()
        n_nodes.strip()
        self.n_nodes = int(n_nodes)

        #Read in points into array
        node_loc = []
        for n in range(self.n_nodes):
            #read line and remove preceeding/trailing whitespace
            line = mesh_file.readline()
            line = line.strip()
            #Split line on spaces into list
            rows = line.split()

            #cast elements of list from strings to int/float
            #rows[0] = node number
            #rows[1:] = x,y,z coords of node
            rows[0] = int(rows[0])
            rows[1] = float(rows[1])
            rows[2] = float(rows[2])
            rows[3] = float(rows[3])

            #Append to list of node locations
            node_loc.append(rows)
        self.np_node_loc = sp.array(node_loc)

    def parse_elements(self, mesh_file):
        #Read number of elements and cast to int
        n_ele = mesh_file.readline()
        n_ele = n_ele.strip()
        self.n_ele = int(n_ele)

        self.ele = []
        #Structure of elements in msh file
        # [element number] [element type] [number of tags] <tags> [node number list]
        # tags generally are the number of the containing physical group
        # and then the number of the containing geometrical group
        for m in range(self.n_ele):
            line = mesh_file.readline()
            line = line.strip()
            other_rows = line.split()
            for count in range(len(other_rows)):
               other_rows[count]=int(other_rows[count])
            self.ele.append(other_rows)

    #################################
    ## Constructing helper arrays
    #################################

    def sort_elements(self):
        self.tri_ele = []
        self.bound_ele=[]
        self.corner_ele=[]
        for element in self.ele:
          if element[1] == 2:
              #Triangle
              self.tri_ele.append(element)
          elif element[1] == 1:
              #Boundary line
              self.bound_ele.append(element)
          else:
              #Boundary node
              self.corner_ele.append(element)
    def get_boundary_points(self, groupID = 0):
        #Search through boundary elements to find elements in the correct physical group
        N_bound_ele = [];
        for element in self.bound_ele:
            if element[3]==groupID or groupID == 0:
                N_bound_ele.append(element)

        # Search through list of boundary lines to find the points on this boundary
        # (Last two entries on line are endpoints of line)
        N_boundary_points = [];
        for bound_ele in N_bound_ele:
            N_boundary_points.append(bound_ele[-2:])

        #flatten array and remove duplicates
        N_flat = list(itertools.chain.from_iterable(N_boundary_points))
        N_flat = list(set(sorted(N_flat)))

        #Change numbering on elements so that they match the index in the node location array
        #(msh file counts from 1)
        np_N_bound_points = sp.array(N_flat)-1
        return np_N_bound_points


    #################################
    ## Constructing helper arrays
    #################################

    def find_node_position(self, nodeID):
        return self.np_node_loc[sp.where(self.np_node_loc[:,0]==nodeID),[1,2]].flatten()

    #Given a node number return its neighboring nodes
    def neb(self, node_int_number):
        neb_a = self.np_tri_ele[sp.where(self.np_tri_ele[:,-3]==node_int_number)][:,[-2,-1]]
        neb_b = self.np_tri_ele[sp.where(self.np_tri_ele[:,-2]==node_int_number)][:,[-3,-1]]
        neb_c = self.np_tri_ele[sp.where(self.np_tri_ele[:,-1]==node_int_number)][:,[-3,-2]]
        neb_fill = sp.unique(sp.concatenate((neb_a,neb_b,neb_c),axis=0))
        #Append zeros so neb_fill is of size 8
        neb_fill = sp.append(neb_fill, [0] * (8 - neb_fill.size))
        return neb_fill

    #The length between two nodes
    def lij(self, nodei,nodej):
        ix,iy = self.find_node_position(nodei)
        jx,jy = self.find_node_position(nodej)
        length = sp.sqrt((ix-jx)**2 + (iy-jy)**2)
        return length

    #The internal angle formed between two nodes
    def int_angle(self, nodei,nodej,nodek):
        A = self.lij(nodei,nodej)**2 + self.lij(nodei,nodek)**2 - self.lij(nodej,nodek)**2
        B = 2*self.lij(nodei,nodej)*self.lij(nodei,nodek)
        int_angle = sp.arccos(A/B)
        return int_angle

    #Minimum interior angle
    def min_int_angle(self):
        min_int_angle = sp.zeros(self.np_tri_ele.shape[0])
        tri_int_holder = sp.zeros(3)
        triangles=self.np_tri_ele[:,[-3,-2,-1]]

        for n in range(self.np_tri_ele.shape[0]):
            tri_int_holder[0]=self.int_angle(triangles[n,0],triangles[n,1],triangles[n,2])
            tri_int_holder[1]=self.int_angle(triangles[n,1],triangles[n,2],triangles[n,0])
            tri_int_holder[2]=self.int_angle(triangles[n,2],triangles[n,0],triangles[n,1])
            min_int_angle[n]=tri_int_holder.min()
        return min_int_angle

    #Signed area of a triangle
    def signed_area(self, nodei,nodej,nodek):
        x1,y1=self.find_node_position(nodei)
        x2,y2=self.find_node_position(nodej)
        x3,y3=self.find_node_position(nodek)
        A = 0.5*(-1.0*x2*y1 + x3*y1 + x1*y2 - x3*y2 - x1*y3 + x2*y3)
        return A

    #Tot signed area.  Meaning the sum of all the triangles over the entire mesh.
    def tot_signed_area(self):
        tot_signed_area = sp.zeros(self.np_tri_ele.shape[0])
        # array of arrays of nodes at triangle corners
        triangles = self.np_tri_ele[:,[-3,-2,-1]]

        for n in range(self.np_tri_ele.shape[0]):
            #find area of each triangle and place it in array
            tot_signed_area[n]=self.signed_area(triangles[n][0],triangles[n][1],triangles[n][2])
        return tot_signed_area


    def area(self, nodei_index,nodej_index,nodek_index):
        s = (self.lij(nodei_index,nodej_index) + self.lij(nodei_index,nodek_index) + self.lij(nodej_index,nodek_index))/2.0
        area = sp.sqrt(s*(s-self.lij(nodei_index,nodej_index))*(s-self.lij(nodei_index,nodek_index))*(s-self.lij(nodej_index,nodek_index)))
        return area


    #################################
    ## Constructing Spring matrix
    #################################
    #Local lineal stiffness K
    def K_lin(self, nodei,nodej):
        K_lin = sp.zeros((4,4))
        ix,iy=self.find_node_position(nodei)
        jx,jy=self.find_node_position(nodej)
        y_edge = jy-iy; x_edge = jx-ix;
        alpha = sp.arctan2(y_edge,x_edge)

        K_lin[0,0]=(sp.cos(alpha))**2
        K_lin[0,1]=sp.sin(alpha)*sp.cos(alpha)
        K_lin[0,2]=-1.0*(sp.cos(alpha))**2
        K_lin[0,3]=-1.0*sp.sin(alpha)*sp.cos(alpha)


        K_lin[1,0]=sp.sin(alpha)*sp.cos(alpha)
        K_lin[1,1]=(sp.sin(alpha))**2
        K_lin[1,2]=-1.0*sp.sin(alpha)*sp.cos(alpha)
        K_lin[1,3]=-1.0*(sp.sin(alpha))**2

        K_lin[2,0]=-1.0*(sp.cos(alpha))**2
        K_lin[2,1]=-1.0*sp.sin(alpha)*sp.cos(alpha)
        K_lin[2,2]=(sp.cos(alpha))**2
        K_lin[2,3]=sp.sin(alpha)*sp.cos(alpha)


        K_lin[3,0]=-1.0*sp.sin(alpha)*sp.cos(alpha)
        K_lin[3,1]=-1.0*(sp.sin(alpha))**2
        K_lin[3,2]=sp.sin(alpha)*sp.cos(alpha)
        K_lin[3,3]=(sp.sin(alpha))**2

        K_lin = (1.0/self.lij(nodei,nodej))*K_lin

        return K_lin

    def tors(self, nodei,nodej,nodek):
        i=nodei;j=nodej;k=nodek;
        tors = (self.lij(i,j)**2 * self.lij(i,k)**2)/(4*self.area(i,j,k)**2)
        return tors

    def neb_tri(self, nodei,nodej):
        if nodei == nodej:
            print ("nodei and nodej must be distinct.\n")
            return sp.NaN
        #For edges along a boundary there is only one triangle per edge
        elif sp.any(sp.array(self.bound_ele)[:,0]==nodei) and sp.any(sp.array(self.bound_ele)[:,0]==nodej):
            #k1 = 3rd node in triangle
            k1=sp.trim_zeros(sp.intersect1d(self.neb(nodei),self.neb(nodej)))[0]
            K_tor1 = self.K_tor(nodei,nodej,k1)[[0,1,2,3],:]
            return k1,K_tor1
        #For edges on the interior there are two triangles per edge
        else:
            k1,k2=sp.array(sp.trim_zeros(sp.intersect1d(self.neb(nodei),self.neb(nodej)))).flatten()
            #k1,k2 = 3rd node in triangles sharing edge from nodei to nodej
            K_tor1 = self.K_tor(nodei,nodej,k1)[[0,1,2,3],:]
            K_tor2 = self.K_tor(nodei,nodej,k2)[[0,1,2,3],:]
            return k1,K_tor1,k2,K_tor2

    def C(self, nodei,nodej,nodek):
        C_mat = sp.zeros((3,3))
        i=nodei;j=nodej;k=nodek;
        C_mat[0,0]=self.tors(i,j,k)
        C_mat[1,1]=self.tors(j,k,i)
        C_mat[2,2]=self.tors(k,i,j)
        return C_mat

    def K_tor(self, nodei,nodej,nodek):
        i=nodei;j=nodej;k=nodek;
        # R^-1 @ C @ R
        A = sp.dot(sp.transpose(self.rot(i,j,k)),self.C(i,j,k))
        K_tor = sp.dot(A,self.rot(i,j,k))
        return K_tor

    def x_disp(self, nodei,nodej):
        x_hold=sp.zeros(2)
        x_hold[0]=self.np_node_loc[sp.where(self.np_node_loc[:,0]==nodei),1].flatten()[0]
        x_hold[1]=self.np_node_loc[sp.where(self.np_node_loc[:,0]==nodej),1].flatten()[0]
        x_disp = x_hold[1]-x_hold[0]
        return x_disp

    def y_disp(self, nodei,nodej):
        y_hold=sp.zeros(2)
        y_hold[0]=self.np_node_loc[sp.where(self.np_node_loc[:,0]==nodei),2].flatten()[0]
        y_hold[1]=self.np_node_loc[sp.where(self.np_node_loc[:,0]==nodej),2].flatten()[0]
        y_disp = y_hold[1]-y_hold[0]
        return y_disp

    def a_rot(self, nodei,nodej):
        a_rot = self.x_disp(nodei,nodej)/(self.lij(nodei,nodej)**2)
        return a_rot

    def b_rot(self, nodei,nodej):
        b_rot = self.y_disp(nodei,nodej)/(self.lij(nodei,nodej)**2)
        return b_rot

    def rot(self, nodei,nodej,nodek):
        rot_mat = sp.zeros((3,6))
        i=nodei;j=nodej;k=nodek;

        rot_mat[0,0]=self.b_rot(i,k)-self.b_rot(i,j)
        rot_mat[0,1]=self.a_rot(i,j)-self.a_rot(i,k)
        rot_mat[0,2]=self.b_rot(i,j)
        rot_mat[0,3]=-1.0*self.a_rot(i,j)
        rot_mat[0,4]=-1.0*self.b_rot(i,k)
        rot_mat[0,5]=self.a_rot(i,k)

        rot_mat[1,0]=-1.0*self.b_rot(j,i)
        rot_mat[1,1]=self.a_rot(j,i)
        rot_mat[1,2]=self.b_rot(j,i)-self.b_rot(j,k)
        rot_mat[1,3]=self.a_rot(j,k)-self.a_rot(j,i)
        rot_mat[1,4]=self.b_rot(j,k)
        rot_mat[1,5]=-1.0*self.a_rot(j,k)

        rot_mat[2,0]=self.b_rot(k,i)
        rot_mat[2,1]=-1.0*self.a_rot(k,i)
        rot_mat[2,2]=-1.0*self.b_rot(k,j)
        rot_mat[2,3]=self.a_rot(k,j)
        rot_mat[2,4]=self.b_rot(k,j)-self.b_rot(k,i)
        rot_mat[2,5]=self.a_rot(k,i)-self.a_rot(k,j)

        return rot_mat

    #Placing K_lin in the Global K
    def K_lin_loc(self, nodei):
        K_lin_loc = sp.zeros([self.n_nodes*2,self.n_nodes*2])

        for n in self.neb(nodei)[sp.nonzero(self.neb(nodei))[0]]:

            local_k_mat = self.K_lin(nodei, n)

            K_lin_loc[2*nodei-2,2*nodei-2] += local_k_mat[0,0]
            K_lin_loc[2*nodei-2,2*nodei-1] += local_k_mat[0,1]
            K_lin_loc[2*nodei-2,(n*2)-2] += local_k_mat[0,2]
            K_lin_loc[2*nodei-2,(n*2)-1] += local_k_mat[0,3]

            K_lin_loc[2*nodei-1,2*nodei-2] += local_k_mat[1,0]
            K_lin_loc[2*nodei-1,2*nodei-1] += local_k_mat[1,1]
            K_lin_loc[2*nodei-1,(n*2)-2] += local_k_mat[1,2]
            K_lin_loc[2*nodei-1,(n*2)-1] += local_k_mat[1,3]

            K_lin_loc[2*n-2,2*nodei-2] += local_k_mat[2,0]
            K_lin_loc[2*n-2,2*nodei-1] += local_k_mat[2,1]
            K_lin_loc[2*n-2,(n*2)-2] += local_k_mat[2,2]
            K_lin_loc[2*n-2,(n*2)-1] += local_k_mat[2,3]

            K_lin_loc[2*n-1,2*nodei-2] += local_k_mat[3,0]
            K_lin_loc[2*n-1,2*nodei-1] += local_k_mat[3,1]
            K_lin_loc[2*n-1,(n*2)-2] += local_k_mat[3,2]
            K_lin_loc[2*n-1,(n*2)-1] += local_k_mat[3,3]

            #Add contributions from Torsion

            #if boundary node
            if len(self.neb_tri(nodei,n))==2:
                k1 = self.neb_tri(nodei,n)[0].astype(int)

                tor_K = self.neb_tri(nodei, n)[1]

                K_lin_loc[2*nodei-2,2*nodei-2] += tor_K[0,0]
                K_lin_loc[2*nodei-2,2*nodei-1] += tor_K[0,1]
                K_lin_loc[2*nodei-2,(n*2)-2] += tor_K[0,2]
                K_lin_loc[2*nodei-2,(n*2)-1] += tor_K[0,3]
                K_lin_loc[2*nodei-2,(k1*2)-2] += tor_K[0,4]
                K_lin_loc[2*nodei-2,(k1*2)-1] += tor_K[0,5]

                K_lin_loc[2*nodei-1,2*nodei-2] += tor_K[1,0]
                K_lin_loc[2*nodei-1,2*nodei-1] += tor_K[1,1]
                K_lin_loc[2*nodei-1,(n*2)-2] += tor_K[1,2]
                K_lin_loc[2*nodei-1,(n*2)-1] += tor_K[1,3]
                K_lin_loc[2*nodei-1,(k1*2)-2] += tor_K[1,4]
                K_lin_loc[2*nodei-1,(k1*2)-1] += tor_K[1,5]

                K_lin_loc[2*n-2,2*nodei-2] += tor_K[2,0]
                K_lin_loc[2*n-2,2*nodei-1] += tor_K[2,1]
                K_lin_loc[2*n-2,(n*2)-2] += tor_K[2,2]
                K_lin_loc[2*n-2,(n*2)-1] += tor_K[2,3]
                K_lin_loc[2*n-2,(k1*2)-2] += tor_K[2,4]
                K_lin_loc[2*n-2,(k1*2)-1] += tor_K[2,5]

                K_lin_loc[2*n-1,2*nodei-2] += tor_K[3,0]
                K_lin_loc[2*n-1,2*nodei-1] += tor_K[3,1]
                K_lin_loc[2*n-1,(n*2)-2] += tor_K[3,2]
                K_lin_loc[2*n-1,(n*2)-1] += tor_K[3,3]
                K_lin_loc[2*n-1,(k1*2)-2] += tor_K[3,4]
                K_lin_loc[2*n-1,(k1*2)-1] += tor_K[3,5]


            #else if internal node
            else:
                k1 = self.neb_tri(nodei,n)[0]
                k2 = self.neb_tri(nodei,n)[2]

                tor_K1 = self.neb_tri(nodei, n)[1]
                tor_K2 = self.neb_tri(nodei, n)[3]


                K_lin_loc[2*nodei-2,2*nodei-2] += tor_K1[0,0]
                K_lin_loc[2*nodei-2,2*nodei-1] += tor_K1[0,1]
                K_lin_loc[2*nodei-2,(n*2)-2] += tor_K1[0,2]
                K_lin_loc[2*nodei-2,(n*2)-1] += tor_K1[0,3]
                K_lin_loc[2*nodei-2,(k1*2)-2] += tor_K1[0,4]
                K_lin_loc[2*nodei-2,(k1*2)-1] += tor_K1[0,5]

                K_lin_loc[2*nodei-1,2*nodei-2] += tor_K1[1,0]
                K_lin_loc[2*nodei-1,2*nodei-1] += tor_K1[1,1]
                K_lin_loc[2*nodei-1,(n*2)-2] += tor_K1[1,2]
                K_lin_loc[2*nodei-1,(n*2)-1] += tor_K1[1,3]
                K_lin_loc[2*nodei-1,(k1*2)-2] += tor_K1[1,4]
                K_lin_loc[2*nodei-1,(k1*2)-1] += tor_K1[1,5]

                K_lin_loc[2*n-2,2*nodei-2] += tor_K1[2,0]
                K_lin_loc[2*n-2,2*nodei-1] += tor_K1[2,1]
                K_lin_loc[2*n-2,(n*2)-2] += tor_K1[2,2]
                K_lin_loc[2*n-2,(n*2)-1] += tor_K1[2,3]
                K_lin_loc[2*n-2,(k1*2)-2] += tor_K1[2,4]
                K_lin_loc[2*n-2,(k1*2)-1] += tor_K1[2,5]

                K_lin_loc[2*n-1,2*nodei-2] += tor_K1[3,0]
                K_lin_loc[2*n-1,2*nodei-1] += tor_K1[3,1]
                K_lin_loc[2*n-1,(n*2)-2] += tor_K1[3,2]
                K_lin_loc[2*n-1,(n*2)-1] += tor_K1[3,3]
                K_lin_loc[2*n-1,(k1*2)-2] += tor_K1[3,4]
                K_lin_loc[2*n-1,(k1*2)-1] += tor_K1[3,5]


                K_lin_loc[2*nodei-2,2*nodei-2] += tor_K2[0,0]
                K_lin_loc[2*nodei-2,2*nodei-1] += tor_K2[0,1]
                K_lin_loc[2*nodei-2,(n*2)-2] += tor_K2[0,2]
                K_lin_loc[2*nodei-2,(n*2)-1] += tor_K2[0,3]
                K_lin_loc[2*nodei-2,(k2*2)-2] += tor_K2[0,4]
                K_lin_loc[2*nodei-2,(k2*2)-1] += tor_K2[0,5]

                K_lin_loc[2*nodei-1,2*nodei-2] += tor_K2[1,0]
                K_lin_loc[2*nodei-1,2*nodei-1] += tor_K2[1,1]
                K_lin_loc[2*nodei-1,(n*2)-2] += tor_K2[1,2]
                K_lin_loc[2*nodei-1,(n*2)-1] += tor_K2[1,3]
                K_lin_loc[2*nodei-1,(k2*2)-2] += tor_K2[1,4]
                K_lin_loc[2*nodei-1,(k2*2)-1] += tor_K2[1,5]

                K_lin_loc[2*n-2,2*nodei-2] += tor_K2[2,0]
                K_lin_loc[2*n-2,2*nodei-1] += tor_K2[2,1]
                K_lin_loc[2*n-2,(n*2)-2] += tor_K2[2,2]
                K_lin_loc[2*n-2,(n*2)-1] += tor_K2[2,3]
                K_lin_loc[2*n-2,(k2*2)-2] += tor_K2[2,4]
                K_lin_loc[2*n-2,(k2*2)-1] += tor_K2[2,5]

                K_lin_loc[2*n-1,2*nodei-2] += tor_K2[3,0]
                K_lin_loc[2*n-1,2*nodei-1] += tor_K2[3,1]
                K_lin_loc[2*n-1,(n*2)-2] += tor_K2[3,2]
                K_lin_loc[2*n-1,(n*2)-1] += tor_K2[3,3]
                K_lin_loc[2*n-1,(k2*2)-2] += tor_K2[3,4]
                K_lin_loc[2*n-1,(k2*2)-1] += tor_K2[3,5]

        return K_lin_loc

    def build_spring_matrix(self):
        #Matrix of spring coefficients due to contributions from all nodes
        #contains k_x and k_y for each node, so twice as many entries as nodes
        K_glob = sp.zeros([2*self.n_nodes,2*self.n_nodes])

        # sp.linspace = [1,2,3....,n_nodes]
        for n in sp.linspace(1, self.n_nodes, self.n_nodes).astype(int):
            #print("Node: {}".format(n))
            #Adds the contribution from the springs attached to the nth node to the global array
            K_glob = self.K_lin_loc(n)+K_glob

            #We want the boundary to be unchanged (other than due to the deformation)
            #Clear the top 2*n_nodes_ext rows and put 1 in the diagonal elements
        K_glob_ones = K_glob.copy()
        K_glob_ones[0:2*self.n_nodes_ext,:]=0.0
        for n in range(2*self.n_nodes_ext):
            K_glob_ones[n,n]=1.0

        return K_glob_ones

    def print_spring_matrix(self):
        return self.build_spring_matrix()

    #################################
    ## Constructing deformation vector
    #################################

    def set_deformed_configuration(self, deformed_filename):
        #Read another file of the same mesh but deformed boundary
        deformed_mesh_file = open(deformed_filename,'r')

        #Ignore header info
        deformed_mesh_file.readline()
        deformed_mesh_file.readline()
        deformed_mesh_file.readline()
        deformed_mesh_file.readline()

        self.parse_nodes(deformed_mesh_file)

        #Determining N,S,W,E boundaries
        #N = physical group 3, S = 1, W = 4, E = 2;
        #Pulls out lines of np_node_loc corresponding to physical groupID
        #Also shows locations of nodes compared to np_N_bound_points
        np_N_comp_node_loc = self.np_node_loc[self.get_boundary_points(10),:]
        #np_S_comp_node_loc = self.np_node_loc[self.get_boundary_points(1),:]
        #np_E_comp_node_loc = self.np_node_loc[self.get_boundary_points(2),:]
        #np_W_comp_node_loc = self.np_node_loc[self.get_boundary_points(4),:]
        np_comp_node_loc = self.np_node_loc[self.get_boundary_points(),:]

        self.np_phys_N = np_N_comp_node_loc.copy()
        #self.np_phys_S = np_S_comp_node_loc.copy()
        #self.np_phys_W = np_W_comp_node_loc.copy()
        #self.np_phys_E = np_E_comp_node_loc.copy()

    def build_deformation_vector(self):
        disp = sp.zeros([self.n_nodes,2])#Displacement

        n = 0
        # m pulls out the node number of each node in np_phys_N
        for m in self.np_phys_N[:,0].astype(int):
            #Displacement in x coord
            disp[m-1,0]=self.np_phys_N[n,1] - self.np_phys_N_prev[n,0]
            #Displacement in y coord
            disp[m-1,1]=self.np_phys_N[n,2] - self.np_phys_N_prev[n,1]#disp is 'f' in this KU=f matrix equation
            n=n+1

        #print("Counter: {}\n".format(counter))
        f_glob = disp.copy()

        #construct array of arrays of displacements
        #then flatten array to get vector of coords
        f_glob = sp.ravel(sp.column_stack((f_glob[:,0],f_glob[:,1])))
        return f_glob

    #################################
    ## Relaxing Mesh
    #################################

    def relax_mesh(self):

        K_glob_ones = self.build_spring_matrix()
        f_glob = self.build_deformation_vector()
        #Solves a*x = b for x
        #where a = K_glob_ones, b = f_glob
        orig_disp = sp.linalg.solve(K_glob_ones,f_glob)#This is a direct solver, other iterative methods could be used

        #Reshape vector into 2 columns where each row is a the displacement of the corresponding nodes
        solve_disp_reshape = orig_disp.reshape([self.n_nodes,2])

        self.np_node_loc[:,1]= self.np_node_loc[:,1] + solve_disp_reshape[:,0]#Smoothed X-coordiantes = old X-coordinates + solved_x-displacemnt
        self.np_node_loc[:,2]= self.np_node_loc[:,2] + solve_disp_reshape[:,1]#Smoothed Y-coordinates = old Y-coordiantes + solved_y-displacement

        self.np_phys_N_prev = self.np_phys_N[:,1:3].copy() #The old boundary is now the new one, remember it is all about displacements.

    #################################
    ## Tangle check
    #################################

    def tangle_check(self):
        # if a triangle's area has swapped sign, a tangle has appeared
        diff_signed_area = sp.sign(self.tot_signed_area())-sp.sign(self.signed_area_prev)#This just checks for tangles
        # set tangle_counter to the timestep where the tangle appeared
        if sp.any(diff_signed_area)!=0 and self.tangle_counter==0:
            self.tangle_counter = self.counter
        self.signed_area_prev = self.tot_signed_area()


    def plot_mesh(self, counter):
        triangles = self.np_tri_ele[:,-3:]-1.0
        plt.figure()
        plt.triplot(self.np_node_loc[:,1], self.np_node_loc[:,2],'-ko',triangles = triangles, markersize=2)
        plt.tripcolor(self.np_node_loc[:,1], self.np_node_loc[:,2], triangles = triangles, facecolors = self.min_int_angle(), cmap=cm.rainbow_r, vmin=0, vmax=sp.pi/3)
        cbar = plt.colorbar(ticks=[0, sp.pi/12,sp.pi/6,sp.pi/4, sp.pi/3])
        cbar.ax.set_yticklabels([r'$0$', r'$\frac{\pi}{12}$',r'$\frac{\pi}{6}$',r'$\frac{\pi}{4}$',r'$\frac{\pi}{3}$'],fontsize=14)  # vertically oriented colorbar
        plt.axes().set_aspect('equal')
        plt.xlim(-2,2)
        plt.ylim(-2,2)
        plt.xlabel(r'$X$',fontsize=16)
        plt.ylabel(r'$Y$',fontsize=16)
        plt.title(r'$\textrm{S}_{l}$',fontsize=22)
        plt.text(0.7,1.3,r'$n = {}$'.format(counter), fontsize=14)

        if self.tangle_counter != 0:
            plt.text(0.7,1.2,r'$n_{} = {}$'.format('t',self.tangle_counter),fontsize=14)

        plt.savefig('./linfigures/2d_lin_direct_{}.png'.format(counter),bbox_inches='tight',dpi = 300)

test = Mesh("test.msh")
