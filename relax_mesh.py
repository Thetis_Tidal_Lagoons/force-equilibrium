import sys
import lin_spring as lsp

mesh = lsp.Mesh(sys.argv[1])
mesh.set_deformed_configuration(sys.argv[2])
mesh.relax_mesh()
mesh.save_mesh("/home/tom/output_file.msh")
mesh.plot_mesh(1)
