# -*- coding: utf-8 -*-
"""
Purpose: 2D Lineal Spring Solver
Author: T. M. McManus
Date: 21-9-16
"""

#import matplotlib
#matplotlib.use('Agg')
import scipy as sp
import matplotlib.pyplot as plt
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import itertools
import time
import readline
from matplotlib import rc
import matplotlib.cm as cm
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
rc('text', usetex=True)

readline.parse_and_bind("tab: complete")

#fileinput.input()
mesh_file = raw_input("Enter mesh file:")
#mesh_file = fileinput.filename()
print(mesh_file)
f = open(mesh_file,'r')
start = time.time()
plt.close('all')


#f = open('comp_he_10.msh', 'r')
#f = open(mesh_file,'r')

# Strip off preamble
f.readline()
f.readline()
f.readline()
f.readline()

#Read the number of nodes and cast to int
n_nodes = f.readline()
n_nodes.strip()
n_nodes = int(n_nodes)

#Read in points into array
node_loc = []
for n in range(n_nodes):
    #read line and remove preceeding/trailing whitespace
    line = f.readline()
    line = line.strip()
    #Split line on spaces into list
    rows = line.split()

    #cast elements of list from strings to int/float
    #rows[0] = node number
    #rows[1:] = x,y,z coords of node
    rows[0] = int(rows[0])
    rows[1] = float(rows[1])
    rows[2] = float(rows[2])
    rows[3] = float(rows[3])

    #Append to list of node locations
    node_loc.append(rows)
np_node_loc = sp.array(node_loc)

#Strip $EndNodes
f.readline()
#Strip $Elements
f.readline()

#Read number of elements and cast to int
n_ele = f.readline()
n_ele = n_ele.strip()
n_ele = int(n_ele)

ele = []

#Structure of elements in msh file
# [element number] [element type] [number of tags] <tags> [node number list]
# tags generally are the number of the containing physical group
# and then the number of the containing geometrical group
for m in range(n_ele):
    line = f.readline()
    line = line.strip()
    other_rows = line.split()
    for count in range(len(other_rows)):
       other_rows[count]=int(other_rows[count])
    ele.append(other_rows)
f.close()

#Go throught every element and determine if it is a triangle
#if it is not, remove it from the list of triangular elements

tri_ele = []
bound_ele=[]
corner_ele=[]
for element in ele:
  if element[1] == 2:
      #Triangle
      tri_ele.append(element)
  elif element[1] == 1:
      #Boundary line
      bound_ele.append(element)
  else:
      #Boundary node
      corner_ele.append(element)

boundary_points = []

def get_boundary_points(bound_ele, groupID = 0):
    #Search through boundary elements to find elements in the correct physical group
    N_bound_ele = [];
    for element in bound_ele:
        if element[3]==groupID or groupID == 0:
            N_bound_ele.append(element)

    # Search through list of boundary lines to find the points on this boundary
    # (Last two entries on line are endpoints of line)
    N_boundary_points = [];
    for bound_ele in N_bound_ele:
        N_boundary_points.append(bound_ele[-2:])

    #flatten array and remove duplicates
    N_flat = list(itertools.chain.from_iterable(N_boundary_points))
    N_flat = list(set(sorted(N_flat)))

    #Change numbering on elements so that they match the index in the node location array
    #(msh file counts from 1)
    np_N_bound_points = sp.array(N_flat)-1
    return np_N_bound_points

#Determining N,S,W,E boundaries
#N = physical group 3, S = 1, W = 4, E = 2;
#Pulls out lines of np_node_loc corresponding to physical groupID
#Also shows locations of nodes compared to np_N_bound_points
np_N_comp_node_loc = np_node_loc[get_boundary_points(bound_ele,3),:]
np_S_comp_node_loc = np_node_loc[get_boundary_points(bound_ele,1),:]
np_E_comp_node_loc = np_node_loc[get_boundary_points(bound_ele,2),:]
np_W_comp_node_loc = np_node_loc[get_boundary_points(bound_ele,4),:]
np_comp_node_loc = np_node_loc[get_boundary_points(bound_ele),:]

np_phys_N = np_N_comp_node_loc.copy()
np_phys_S = np_S_comp_node_loc.copy()
np_phys_W = np_W_comp_node_loc.copy()
np_phys_E = np_E_comp_node_loc.copy()

#Calculate number of exterior (boundary) and internal points
n_nodes_ext = get_boundary_points(bound_ele).size
n_nodes_int = n_nodes - n_nodes_ext

#Removes locations of boundary points from a copy of np_node_loc to give locations of interior points
node_int_loc = sp.delete(np_node_loc,get_boundary_points(boundary_points),0)

#Creates copy of tri_ele as a numpy array
np_tri_ele = sp.array(tri_ele)

def find_node_position(nodeID):
    return np_node_loc[sp.where(np_node_loc[:,0]==nodeID),[1,2]].flatten()

#Given a node number return its neighboring nodes
def neb(node_int_number):
    neb_a = np_tri_ele[sp.where(np_tri_ele[:,-3]==node_int_number)][:,[-2,-1]]
    neb_b = np_tri_ele[sp.where(np_tri_ele[:,-2]==node_int_number)][:,[-3,-1]]
    neb_c = np_tri_ele[sp.where(np_tri_ele[:,-1]==node_int_number)][:,[-3,-2]]
    neb_fill = sp.unique(sp.concatenate((neb_a,neb_b,neb_c),axis=0))
    #Append zeros so neb_fill is of size 8
    neb_fill = sp.append(neb_fill, [0] * (8 - neb_fill.size))
    return neb_fill

#The length between two nodes
def lij(nodei,nodej):
    ix,iy = find_node_position(nodei)
    jx,jy = find_node_position(nodej)
    length = sp.sqrt((ix-jx)**2 + (iy-jy)**2)
    return length

#The internal angle formed between two nodes
def int_angle(nodei,nodej,nodek):
    A = lij(nodei,nodej)**2 + lij(nodei,nodek)**2 - lij(nodej,nodek)**2
    B = 2*lij(nodei,nodej)*lij(nodei,nodek)
    int_angle = sp.arccos(A/B)
    return int_angle

#Minimum interior angle
def min_int_angle(np_tri_ele):
    min_int_angle = sp.zeros(np_tri_ele.shape[0])
    tri_int_holder = sp.zeros(3)
    triangles=np_tri_ele[:,[-3,-2,-1]]

    for n in range(np_tri_ele.shape[0]):
        tri_int_holder[0]=int_angle(triangles[n,0],triangles[n,1],triangles[n,2])
        tri_int_holder[1]=int_angle(triangles[n,1],triangles[n,2],triangles[n,0])
        tri_int_holder[2]=int_angle(triangles[n,2],triangles[n,0],triangles[n,1])
        min_int_angle[n]=tri_int_holder.min()
    return min_int_angle

#Signed area of a triangle
def signed_area(nodei,nodej,nodek):
    x1,y1=find_node_position(nodei)
    x2,y2=find_node_position(nodej)
    x3,y3=find_node_position(nodek)
    A = 0.5*(-1.0*x2*y1 + x3*y1 + x1*y2 - x3*y2 - x1*y3 + x2*y3)
    return A

#Tot signed area.  Meaning the sum of all the triangles over the entire mesh.
def tot_signed_area(np_tri_ele):
    tot_signed_area = sp.zeros(np_tri_ele.shape[0])
    # array of arrays of nodes at triangle corners
    triangles = np_tri_ele[:,[-3,-2,-1]]

    for n in range(np_tri_ele.shape[0]):
        #find area of each triangle and place it in array
        tot_signed_area[n]=signed_area(triangles[n][0],triangles[n][1],triangles[n][2])
    return tot_signed_area

#Local lineal stiffness K
def K_lin(nodei,nodej):
    K_lin = sp.zeros((4,4))
    ix,iy=find_node_position(nodei)
    jx,jy=find_node_position(nodej)
    y_edge = jy-iy; x_edge = jx-ix;
    alpha = sp.arctan2(y_edge,x_edge)

    K_lin[0,0]=(sp.cos(alpha))**2
    K_lin[0,1]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[0,2]=-1.0*(sp.cos(alpha))**2
    K_lin[0,3]=-1.0*sp.sin(alpha)*sp.cos(alpha)


    K_lin[1,0]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[1,1]=(sp.sin(alpha))**2
    K_lin[1,2]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[1,3]=-1.0*(sp.sin(alpha))**2

    K_lin[2,0]=-1.0*(sp.cos(alpha))**2
    K_lin[2,1]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[2,2]=(sp.cos(alpha))**2
    K_lin[2,3]=sp.sin(alpha)*sp.cos(alpha)


    K_lin[3,0]=-1.0*sp.sin(alpha)*sp.cos(alpha)
    K_lin[3,1]=-1.0*(sp.sin(alpha))**2
    K_lin[3,2]=sp.sin(alpha)*sp.cos(alpha)
    K_lin[3,3]=(sp.sin(alpha))**2

    K_lin = (1.0/lij(nodei,nodej))*K_lin

    return K_lin

#Placing K_lin in the Global K
def K_lin_loc(nodei):
    K_lin_loc = sp.zeros([n_nodes*2,n_nodes*2])

    for n in neb(nodei)[sp.nonzero(neb(nodei))[0]]:

        K_lin_loc[2*nodei-2,2*nodei-2] += K_lin(nodei,n)[0,0]
        K_lin_loc[2*nodei-2,2*nodei-1] += K_lin(nodei,n)[0,1]
        K_lin_loc[2*nodei-2,(n*2)-2] += K_lin(nodei,n)[0,2]
        K_lin_loc[2*nodei-2,(n*2)-1] += K_lin(nodei,n)[0,3]

        K_lin_loc[2*nodei-1,2*nodei-2] += K_lin(nodei,n)[1,0]
        K_lin_loc[2*nodei-1,2*nodei-1] += K_lin(nodei,n)[1,1]
        K_lin_loc[2*nodei-1,(n*2)-2] += K_lin(nodei,n)[1,2]
        K_lin_loc[2*nodei-1,(n*2)-1] += K_lin(nodei,n)[1,3]

        K_lin_loc[2*n-2,2*nodei-2] += K_lin(nodei,n)[2,0]
        K_lin_loc[2*n-2,2*nodei-1] += K_lin(nodei,n)[2,1]
        K_lin_loc[2*n-2,(n*2)-2] += K_lin(nodei,n)[2,2]
        K_lin_loc[2*n-2,(n*2)-1] += K_lin(nodei,n)[2,3]

        K_lin_loc[2*n-1,2*nodei-2] += K_lin(nodei,n)[3,0]
        K_lin_loc[2*n-1,2*nodei-1] += K_lin(nodei,n)[3,1]
        K_lin_loc[2*n-1,(n*2)-2] += K_lin(nodei,n)[3,2]
        K_lin_loc[2*n-1,(n*2)-1] += K_lin(nodei,n)[3,3]

    return K_lin_loc

triangles = np_tri_ele[:,-3:]-1.0
np_node_loc_orig=np_node_loc.copy()

steps = 2 #Number of amplitude steps. Only need two steps here, one for the undeformed boundary, and the second
#for the boundary you desire.

np_phys_N_prev = np_phys_N[:,1:3].copy()
w_0=sp.linspace(0,0.5,steps)#Amplitude as a function of steps
signed_area_prev = tot_signed_area(np_tri_ele)
tangle_counter=0


def plot_mesh(np_node_loc, triangles, np_tri_ele, tangle_counter):
    plt.figure()
    plt.triplot(np_node_loc[:,1],np_node_loc[:,2],'-ko',triangles = triangles,markersize=2)
    plt.tripcolor(np_node_loc[:,1],np_node_loc[:,2],triangles=triangles,facecolors=min_int_angle(np_tri_ele),cmap=cm.rainbow_r,vmin=0,vmax=sp.pi/3)
    cbar = plt.colorbar(ticks=[0, sp.pi/12,sp.pi/6,sp.pi/4, sp.pi/3])
    cbar.ax.set_yticklabels([r'$0$', r'$\frac{\pi}{12}$',r'$\frac{\pi}{6}$',r'$\frac{\pi}{4}$',r'$\frac{\pi}{3}$'],fontsize=14)  # vertically oriented colorbar
    plt.axes().set_aspect('equal')
    plt.xlim(-0.1,1.1)
    plt.ylim(-0.1,1.5)
    plt.xlabel(r'$X$',fontsize=16)
    plt.ylabel(r'$Y$',fontsize=16)
    plt.title(r'$\textrm{S}_{l}$',fontsize=22)
    plt.text(0.7,1.3,r'$n = {}$'.format(counter), fontsize=14)
    if tangle_counter != 0:
        plt.text(0.7,1.2,r'$n_{} = {}$'.format('t',tangle_counter),fontsize=14)
    plt.savefig('./linfigures/2d_lin_direct_{}.png'.format(counter),bbox_inches='tight',dpi = 300)


def build_deformation_vector():
        np_phys_N[:,1] += w_0[counter]*sp.sin(2*sp.pi*np_phys_N[:,1]) #This defines the deformation
        np_phys_N[:,2] += w_0[counter]*sp.sin(2*sp.pi*np_phys_N[:,1]) #This defines the deformation
        #Note the +1 at the end this is simply a shift up of the deformation profile.

        #Build a new boundary by concatenating the deformed boundary in with the others
        #np_phys_boundary = sp.concatenate((np_phys_S,np_phys_E,np_phys_N,np_phys_W),axis=0)
        #Order boundary points correctly
        #np_phys_boundary = np_phys_boundary[np_phys_boundary[:,0].argsort()]
        #np_phys_boundary = sp.delete(np_phys_boundary,[1,3,5,7],0)#1,3,5,7 are the corners which never move

        disp = sp.zeros([n_nodes,2])#Displacement

        n = 0
        # m pulls out the node number of each node in np_phys_N
        for m in np_phys_N[:,0].astype(int):
            #Displacement in x coord
            disp[m-1,0]=np_phys_N[n,1] - np_phys_N_prev[n,0]
            #Displacement in y coord
            disp[m-1,1]=np_phys_N[n,2] - np_phys_N_prev[n,1]#disp is 'f' in this KU=f matrix equation
            n=n+1

        #print("Counter: {}\n".format(counter))
        f_glob = disp.copy()

        #construct array of arrays of displacements
        #then flatten array to get vector of coords
        f_glob = sp.ravel(sp.column_stack((f_glob[:,0],f_glob[:,1])))
        return f_glob

def build_spring_matrix():
        #Matrix of spring coefficients due to contributions from all nodes
        #contains k_x and k_y for each node, so twice as many entries as nodes
        K_glob = sp.zeros([2*n_nodes,2*n_nodes])

        # sp.linspace = [1,2,3....,n_nodes]
        for n in sp.linspace(1,n_nodes,n_nodes).astype(int):
            #print("Node: {}".format(n))
            #Adds the contribution from the springs attached to the nth node to the global array
            K_glob = K_lin_loc(n)+K_glob

        #We want the boundary to be unchanged (other than due to the deformation)
        #Clear the top 2*n_nodes_ext rows and put 1 in the diagonal elements
        K_glob_ones = K_glob.copy()
        K_glob_ones[0:2*n_nodes_ext,:]=0.0
        for n in range(2*n_nodes_ext):
            K_glob_ones[n,n]=1.0

        return K_glob_ones


def relax_mesh():
    #Solves a*x = b for x
    #where a = K_glob_ones, b = f_glob
    orig_disp = sp.linalg.solve(K_glob_ones,f_glob)#This is a direct solver, other iterative methods could be used

    #Reshape vector into 2 columns where each row is a the displacement of the corresponding nodes
    solve_disp_reshape = orig_disp.reshape([n_nodes,2])

    np_node_loc[:,1]=np_node_loc[:,1]+solve_disp_reshape[:,0]#Smoothed X-coordiantes = old X-coordinates + solved_x-displacemnt
    np_node_loc[:,2]=np_node_loc[:,2]+solve_disp_reshape[:,1]#Smoothed Y-coordinates = old Y-coordiantes + solved_y-displacement

    np_phys_N_prev = np_phys_N[:,1:3].copy() #The old boundary is now the new one, remember it is all about displacements.

def tangle_check():
    # if a triangle's area has swapped sign, a tangle has appeared
    diff_signed_area = sp.sign(tot_signed_area(np_tri_ele))-sp.sign(signed_area_prev)#This just checks for tangles
    # set tangle_counter to the timestep where the tangle appeared
    if sp.any(diff_signed_area)!=0 and tangle_counter==0:
        tangle_counter=counter

    signed_area_prev=tot_signed_area(np_tri_ele)

for counter in range(steps):
    
    f_glob = build_deformation_vector()
    K_glob_ones = build_spring_matrix()
    relax_mesh()
    tangle_check()
    plot_mesh(np_node_loc, triangles, np_tri_ele, tangle_counter)
